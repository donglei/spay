﻿module app.service.appchannel;


import app.model;
import hunt;
import app.model.db;
import app.service.base;
import app.helper.snowflake;

class PayAppChannelService
{
	mixin ServiceUtility!("pay_app_channel", PayAppChannelService);
	
	PayAppChannel getByWhere(string[string] whereCondition)
	{
		import std.array, std.conv;
		Appender!string sql = appender!string();
		sql.put("SELECT * ");
		sql.put(" FROM \"");
		sql.put(getTableName());
		sql.put("\" ");
		
		string[] wheres;
		
		foreach(i, col; whereCondition) 
		{
			wheres ~= "\"" ~i~ "\"=" ~ DB().escapeLiteral(col);
		}
		if(wheres.length != 0 )
		{
			sql.put(" WHERE ");
			sql.put(wheres.join(" AND "));
		}
		
		trace("sql:", sql.data);
		auto result = DB().exec(sql.data);
		if(result.rows()==0)
		{
			return null;
		}
		
		
		auto row = result.front;
		import std.typecons, app.utils;
		PayAppChannel appinfo = new PayAppChannel();
		appinfo.appId = row["appid"].as!string().get();
		appinfo.channel = row["channel"].as!string().get();
		string xml = row["config"].as!string().get();
		trace("xml---", xml);
		appinfo.config = xml_aa_wx(xml, "root");
		trace(appinfo.config);
		return appinfo;
	}
}

