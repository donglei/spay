﻿module app.service.appinfo;

import app.model;
import hunt;
import app.model.db;
import app.service.base;
import app.helper.snowflake;

class PayAppinfoService
{
	mixin ServiceUtility!("pay_appinfo", PayAppinfoService);
	
	PayAppInfo getByWhere(string[string] whereCondition)
	{
		import std.array, std.conv;
		Appender!string sql = appender!string();
		sql.put("SELECT * ");
		sql.put(" FROM \"");
		sql.put(getTableName());
		sql.put("\" ");
		
		string[] wheres;
		
		foreach(i, col; whereCondition) 
		{
			wheres ~= "\"" ~i~ "\"=" ~ DB().escapeLiteral(col);
		}
		if(wheres.length != 0 )
		{
			sql.put(" WHERE ");
			sql.put(wheres.join(" AND "));
		}
		
		trace("sql:", sql.data);
		auto result = DB().exec(sql.data);
		if(result.rows()==0)
		{
			return null;
		}
		
		
		auto row = result.front;
		import std.typecons;
		Nullable!string tmp ;
		PayAppInfo appinfo = new PayAppInfo();
		appinfo.appId = row["appid"].as!string().get();
		appinfo.appSecretKey = row["secret_key"].as!string().get();
		appinfo.name = row["name"].as!string().get();
		
		tmp = row["uid"].as!string();
		appinfo.uid = to!ulong(tmp.isNull ? "0" : tmp.get());
		tmp = row["status"].as!string();
		appinfo.status = to!int(tmp.isNull ? "0" : tmp.get());
		return appinfo;
	}
}

