﻿module app.handler.payment.weixinpaypub;
import app.handler.payment.weixinpay;
import std.json;

class WeixinPayPubHandler : WeixinPayHandler
{
	override string getTradeType() {
		return "JSAPI";
	}
	
	override string getOpenId(){
		if("open_id" in orderJson && orderJson["open_id"].type == JSON_TYPE.STRING){
			return orderJson["open_id"].str;
		}
		return "";
	}
}

