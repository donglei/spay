﻿module app.handler.payment.alipaywap;
import app.handler.payment.base;
import std.json;
import app.handler.payment.alipay;
import app.utils;
import std.experimental.logger;

class AlipayWapHandler: AlipayHandler
{
	override string getAlipayProductCode() {
		return "QUICK_WAP_PAY";
	}
	override string getAlipayMethod() {
		return "alipay.trade.wap.pay";
	}
	
	override string getReturnUrl() {
		if("success_url" in orderJson && orderJson["success_url"].type == JSON_TYPE.STRING){
			return orderJson["success_url"].str;
		}
		return "";
	}
}

