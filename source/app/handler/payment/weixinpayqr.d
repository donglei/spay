﻿module app.handler.payment.weixinpayqr;

import app.handler.payment.weixinpay;

///微信二维码扫码支付
class WeixinPayQrHandler:WeixinPayHandler
{
	override string getTradeType(){
		return "NATIVE";
	}
}

