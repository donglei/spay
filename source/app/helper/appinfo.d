﻿module app.helper.appinfo;

public import app.exception;
import std.experimental.logger;
import app.model;

alias SAppInfo = PayAppInfo;


interface IAppInfo{
	SAppInfo getSAppInfoById(string appid);
}
//enum SAppInfo[string] apps = ["c1d4d06048ed7342726955367350baba":SAppInfo("c1d4d06048ed7342726955367350baba", "3234257aa9fe9a460ec5fde040abb53a")];

private __gshared SAppInfo[string] __apps;

///内置的，不是数据库读取
class AppInfoInner : IAppInfo
{
	
	SAppInfo getSAppInfoById(string appid) {
		synchronized(this)
		{
			if(appid in __apps)
				return __apps[appid];
			///从数据库中查找
			import app.service.appinfo;
			PayAppInfo tmp = PayAppinfoService.getInstance.getByWhere(["appid":appid]);
			if(tmp !is null){
				__apps[tmp.appId] = tmp;
				return tmp;
			}
			throwExceptionBuild!"AppIdNotFound"("not support appid : " ~ appid);	
			assert(0);
		}
	}
	
	void clear(){
		synchronized(this)
		{
			__apps.clear();
		}
	}
}

private __gshared string[string][string] __AppChannelsInfo;

///获取应用在此支付渠道下的所有配置信息
string[string] getAppChannelConf(string appid, string channel){
	import std.string;
	string appChannelKey = appid.toLower  ~"@" ~channel.toLower;
	trace("app channel key is:", appChannelKey);
	//synchronized(this)
	{
		if(appChannelKey in __AppChannelsInfo)
			return __AppChannelsInfo[appChannelKey];
		///从数据库中查找
		import app.service.appchannel;
		PayAppChannel tmp = PayAppChannelService.getInstance.getByWhere(["appid":appid, "channel":channel]);
		if(tmp !is null){
			__AppChannelsInfo[appChannelKey] = tmp.config;
			return tmp.config;
		}
		throwExceptionBuild!"NotSupportChannel"("NotSupportChannel : " ~ appChannelKey);	
		assert(0);
	}
}

/*enum AppChannelsInfo = [
 //"AppIDFromSpay@theAppChannel-1" : ["Channel-1-p": "channel-1-v"],
 //"AppIDFromSpay@theAppChannel-2" : ["Channel-2-p": "channel-2-v"],
 //支付宝APP形式
 "c1d4d06048ed7342726955367350baba@alipay":["appid":"2016080300154304","rsa_private_key":"-----BEGIN RSA PRIVATE KEY-----
 MIIEpQIBAAKCAQEA0+1jZB4ed581nQoNkwp0mPQvKYfJbBGNAEbdGIAL1+ZD2Ouk
 iUdWiHc8wWXDiPyif+yOCAY6b65aB4que/xAeTbkbu0igI7QTS77UoEiocLtCjnI
 voRTu4yBBA/bsiaMs8ksjYKP3UQxcz7xa4WvuP8919x8vKsq8nndHCqwN8ic293B
 rtzgV/wJhAdERLaz3p1pj2n7GpsMTvlRugVyGQcW7rytR5w61Lvt+vcOjbf559He
 rQ7a/XObGBZ0v6zSJCr5Vw8dbcCGWDWZjKmKWZ27A6LEIkoq9dLbDEcG0BPTtsYa
 84PtC/O6a3xWgkjKgRuicZUNSxHdUUy+K/Qh8wIDAQABAoIBAEkxEUSAinEx0Shd
 UnbYA5DXtHoZZV0napUP5EgT4QM9iW0fZQHsW1xiId+BL9jdt5mKrzriO8haZMl6
 AezcH8A9TwNobqQLrrEZar8Bzl4jng4MCuKRfQGm8t/eQjfyzGFDN1ngH6OBa7qr
 oGFMGHZB4K/ufD4Et55qrAyQSa/ZviBFTpGNWN2QYSAMf6wgU+DO1OPdfjqNl5qy
 QjFJZLxsRXMqhB7Z1I2YaBfU+pV/kK4eFpE4H3UvIGIj7n4XJYwl7sA3gsj7lC4F
 6VeszcuhLm7xRQfYL2uYxA3Oqw2fy+34eSPLWb/8LkTKFO2zJAXH2otj30dRFr4Z
 hwp4c/ECgYEA8iY61EbcbJ77TZf+6/DqRryLskNDzBp5ST7mpkJHHwTXjoQpM7M7
 rxxVe9uKcw0o44oojYJnE9y4134Jq0E7aeBXgx8xxoyQaC2J+jfczjZwyWIXPwsm
 Lw3R/paMs+fVBLcHlrLz10LSlRNxQuB+igfxItjp/Qw1qQF6yPbNsvcCgYEA4Aye
 tdzFf1bno8fduwPVeX5sC18IgQXswnLh3dl4W6d7Lar6XNAkVfrN2WJY4Jp7EN9i
 GVLYRO4ewMhOVcnAeKC2is0uiW5/B16PwrzPBo4X3ExV7nIVzx6V2JAHFth4ESUb
 +S0gQLo+6XpsSBeZPFLB2ctPwhmIGXKKQm9njeUCgYEAlXXZS9rtBLJgRHVzqCfM
 Qprv0rjH6PvSLs5/SNGR2mh/r/yM/dc8GIpxjQBmBTtzKHbHLwj1HIJZKNEnoKej
 x2bsPQeNDpMGMvcgueuvAy0BEpvT41q7V8G9AtnjwMtwZPef3HlaHlylY9RbTT8J
 e6MJSEwAqOrXWBiMs+v57OMCgYEAou1KdOHI1SMza8yqF5dgI+ulUleXbYwLchPs
 4FGGzs/qKXmOevP5mHS8QPrduudb2xc21UeDcgzfXD3NiWEfkBj+5czzrIkn4woG
 7Qw0WIX4IAF6890OswGA4m1KWnisR3t+7iK8s5U8rriSCZLvoghkY6cPpwy+BhNf
 K5Sr72kCgYEAr1njZlNy9rvU+2sZ9kWalA+/cuoaOY173pecrP5Ll/0B9SilEBJ8
 slhr330xdTE95NlOCujrcSySG8WAH4LpvOJIt6U4P3F7j7Qk2t83F2AIVkP4k56F
 Bo8zL3dIN4Vmfw3zkEHW8RN7NmG78E0rCmfE4k83AwS+qYENp9uIAu4=
 -----END RSA PRIVATE KEY-----", "rsa_public_key":"-----BEGIN PUBLIC KEY-----
 MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0+1jZB4ed581nQoNkwp0
 mPQvKYfJbBGNAEbdGIAL1+ZD2OukiUdWiHc8wWXDiPyif+yOCAY6b65aB4que/xA
 eTbkbu0igI7QTS77UoEiocLtCjnIvoRTu4yBBA/bsiaMs8ksjYKP3UQxcz7xa4Wv
 uP8919x8vKsq8nndHCqwN8ic293BrtzgV/wJhAdERLaz3p1pj2n7GpsMTvlRugVy
 GQcW7rytR5w61Lvt+vcOjbf559HerQ7a/XObGBZ0v6zSJCr5Vw8dbcCGWDWZjKmK
 WZ27A6LEIkoq9dLbDEcG0BPTtsYa84PtC/O6a3xWgkjKgRuicZUNSxHdUUy+K/Qh
 8wIDAQAB
 -----END PUBLIC KEY-----"],
 "c1d4d06048ed7342726955367350baba@alipaywap":["appid":"2016080300154304","rsa_private_key":"-----BEGIN RSA PRIVATE KEY-----
 MIIEpQIBAAKCAQEA0+1jZB4ed581nQoNkwp0mPQvKYfJbBGNAEbdGIAL1+ZD2Ouk
 iUdWiHc8wWXDiPyif+yOCAY6b65aB4que/xAeTbkbu0igI7QTS77UoEiocLtCjnI
 voRTu4yBBA/bsiaMs8ksjYKP3UQxcz7xa4WvuP8919x8vKsq8nndHCqwN8ic293B
 rtzgV/wJhAdERLaz3p1pj2n7GpsMTvlRugVyGQcW7rytR5w61Lvt+vcOjbf559He
 rQ7a/XObGBZ0v6zSJCr5Vw8dbcCGWDWZjKmKWZ27A6LEIkoq9dLbDEcG0BPTtsYa
 84PtC/O6a3xWgkjKgRuicZUNSxHdUUy+K/Qh8wIDAQABAoIBAEkxEUSAinEx0Shd
 UnbYA5DXtHoZZV0napUP5EgT4QM9iW0fZQHsW1xiId+BL9jdt5mKrzriO8haZMl6
 AezcH8A9TwNobqQLrrEZar8Bzl4jng4MCuKRfQGm8t/eQjfyzGFDN1ngH6OBa7qr
 oGFMGHZB4K/ufD4Et55qrAyQSa/ZviBFTpGNWN2QYSAMf6wgU+DO1OPdfjqNl5qy
 QjFJZLxsRXMqhB7Z1I2YaBfU+pV/kK4eFpE4H3UvIGIj7n4XJYwl7sA3gsj7lC4F
 6VeszcuhLm7xRQfYL2uYxA3Oqw2fy+34eSPLWb/8LkTKFO2zJAXH2otj30dRFr4Z
 hwp4c/ECgYEA8iY61EbcbJ77TZf+6/DqRryLskNDzBp5ST7mpkJHHwTXjoQpM7M7
 rxxVe9uKcw0o44oojYJnE9y4134Jq0E7aeBXgx8xxoyQaC2J+jfczjZwyWIXPwsm
 Lw3R/paMs+fVBLcHlrLz10LSlRNxQuB+igfxItjp/Qw1qQF6yPbNsvcCgYEA4Aye
 tdzFf1bno8fduwPVeX5sC18IgQXswnLh3dl4W6d7Lar6XNAkVfrN2WJY4Jp7EN9i
 GVLYRO4ewMhOVcnAeKC2is0uiW5/B16PwrzPBo4X3ExV7nIVzx6V2JAHFth4ESUb
 +S0gQLo+6XpsSBeZPFLB2ctPwhmIGXKKQm9njeUCgYEAlXXZS9rtBLJgRHVzqCfM
 Qprv0rjH6PvSLs5/SNGR2mh/r/yM/dc8GIpxjQBmBTtzKHbHLwj1HIJZKNEnoKej
 x2bsPQeNDpMGMvcgueuvAy0BEpvT41q7V8G9AtnjwMtwZPef3HlaHlylY9RbTT8J
 e6MJSEwAqOrXWBiMs+v57OMCgYEAou1KdOHI1SMza8yqF5dgI+ulUleXbYwLchPs
 4FGGzs/qKXmOevP5mHS8QPrduudb2xc21UeDcgzfXD3NiWEfkBj+5czzrIkn4woG
 7Qw0WIX4IAF6890OswGA4m1KWnisR3t+7iK8s5U8rriSCZLvoghkY6cPpwy+BhNf
 K5Sr72kCgYEAr1njZlNy9rvU+2sZ9kWalA+/cuoaOY173pecrP5Ll/0B9SilEBJ8
 slhr330xdTE95NlOCujrcSySG8WAH4LpvOJIt6U4P3F7j7Qk2t83F2AIVkP4k56F
 Bo8zL3dIN4Vmfw3zkEHW8RN7NmG78E0rCmfE4k83AwS+qYENp9uIAu4=
 -----END RSA PRIVATE KEY-----", "rsa_public_key":"-----BEGIN PUBLIC KEY-----
 MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0+1jZB4ed581nQoNkwp0
 mPQvKYfJbBGNAEbdGIAL1+ZD2OukiUdWiHc8wWXDiPyif+yOCAY6b65aB4que/xA
 eTbkbu0igI7QTS77UoEiocLtCjnIvoRTu4yBBA/bsiaMs8ksjYKP3UQxcz7xa4Wv
 uP8919x8vKsq8nndHCqwN8ic293BrtzgV/wJhAdERLaz3p1pj2n7GpsMTvlRugVy
 GQcW7rytR5w61Lvt+vcOjbf559HerQ7a/XObGBZ0v6zSJCr5Vw8dbcCGWDWZjKmK
 WZ27A6LEIkoq9dLbDEcG0BPTtsYa84PtC/O6a3xWgkjKgRuicZUNSxHdUUy+K/Qh
 8wIDAQAB
 -----END PUBLIC KEY-----"],
 "c1d4d06048ed7342726955367350baba@weixinpay":["appid" : "wx349ef69afd500d25","mch_id":"1383374602","key":"QedVdU65G8HV9Uwg56YrfD12QA89V0d9"],
 "c1d4d06048ed7342726955367350baba@weixinpayqr":["appid" : "wx349ef69afd500d25","mch_id":"1383374602","key":"QedVdU65G8HV9Uwg56YrfD12QA89V0d9"]
 
 ];*/