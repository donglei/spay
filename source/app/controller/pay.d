module app.controller.pay;
import hunt.application;
import std.experimental.logger;
import app.controller.base;
import app.exception;
import std.json;
import app.config;

class PayController : BaseController
{
	mixin MakeController;
	
	@Action
	void order()
	{
		runCatch(&_order,request);
	}
	
	private void _order(Request req)
	{
		string jsonStringData = checkSign(req);
		trace("jsondata:", jsonStringData);
		JSONValue json = parseJSON(jsonStringData);
		JSONValue result;
		enum string str = buildPayOrderHander!(SpayChannel, "json", "result")();
		pragma(msg, str);
		mixin(str);
		trace("result json:", result.toString());
		this.response.json(result);
	}
	
}
