﻿module app.model.db;

public import dpq.connection;
public import dpq.result;
public import dpq.value;
public import std.typecons;

static this()
{
}

Connection newDBConnetion()
{
	return Connection(_dburl);
}

@property Connection * DB()
{
	if(!isInit)
	{
		con = Connection(_dburl);
		isInit = true;
	}
	return &con;
}

void restDBConnection()
{
	con.close();
	con = Connection(_dburl);
}

void closeDBConnection()
{
	con.close();
	isInit = false;
}

void initDBStr(string url)
{
	_dburl = url;
}

private:

__gshared string _dburl ;//= "host=127.0.0.1 dbname=test user=postgres";
Connection con = void;
bool isInit = false;