﻿module app.model;

class PayBill{
	ulong id;
	string outTradeNo;
	string tradeNo;
	int amount;
	string channel;
	string appid;
	ulong createTime;
	string subject;
	string body_;
	ulong paymentTime;
	ulong refundTime;
	short status;
	string passbackParams;
	string notifyUrl;
	ulong notifySuccessTime;
}


class PayAppInfo{
	string appId;
	string appSecretKey;
	string name;
	ulong uid;
	int status;
}
class PayAppChannel{
	string appId;
	string channel;
	string[string] config;
}