﻿module app.config;

///支付渠道
enum SpayChannel:ubyte{
	///支付宝 APP 支付
	Alipay = 0x01,
	///支付宝手机网页支付
	AlipayWap,
	///支付宝电脑网站支付
	//AlipayPcDirect,
	///支付宝当面付，即支付宝扫码支付
	//AlipayQr,
	///微信 APP 支付
	WeixinPay,
	///微信公众号支付
	WeixinPayPub,
	///微信扫码支付
	WeixinPayQr,
	///微信小程序支付
	//WeixinPayLite
}

string buildPayOrderHander(T,string jsonObjName, string resultObjName)() if(is(T == enum))
{
	import std.format;
	string str = format(`if("channel" !in %s){throwExceptionBuild!"Argument"("channel is missing");}`, jsonObjName);
	str ~= `import std.string;
	switch(`~jsonObjName~`["channel"].str.toLower()){`;
	foreach(memberName; __traits(derivedMembers,T)){
		import std.string;
		str ~= `case "` ~memberName.toLower()~`":
		import app.handler.payment.`~memberName.toLower()~`;
		auto handler = new `~memberName~`Handler();
		handler.channel("`~memberName.toLower()~`");
		handler.setOrderJson(`~jsonObjName~`);
		`~resultObjName~ `= handler.pay();
		break;
		`;
	}
	
	str ~=`default: throwExceptionBuild!"NotSupportChannel"("not support channel");break;`;
	str ~=`}`;
	
	return str;
	
}