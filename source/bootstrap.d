module bootstrap;
import std.stdio;
import std.functional;
import std.experimental.logger;
import hunt;
import std.string;
import app.model;
import app.model.db;
import hunt.text.configuration;
version(NotifyClient){}else{
	void main()
	{
		auto conf = Config.app.config;
		setLog(conf.log.level.value(), conf.log.file.value());
		auto app = Application.getInstance();
		initDBStr(buildPGUrl(conf));
		app.run();
	}
}
string buildPGUrl(Configuration conf)
{
	string host = conf.db.pg_host.value;
	string port = conf.db.pg_port.value;
	string dbn = conf.db.pg_dbname.value;
	string usern = conf.db.pg_user.value;
	string pass = conf.db.pg_passworld.value;
	string str = "host=" ~ host;
	if(port.length > 0)
		str ~= " port=" ~ port;
	str ~= " dbname=" ~ dbn;
	if(usern.length > 0)
		str ~= " user=" ~ usern;
	if(pass.length > 0)
		str ~= " password=" ~ pass;
	return str;
}

void setLog(string loglevel = "all", string logfile = "")
{
	
	loglevel = loglevel.length == 0 ? "off" : loglevel;
	
	switch(loglevel)
	{
		case "all":
			globalLogLevel = LogLevel.all;
			break;
		case "critical":
			globalLogLevel = LogLevel.critical;
			break;
		case "error":
			globalLogLevel = LogLevel.error;
			break;
		case "fatal":
			globalLogLevel = LogLevel.fatal;
			break;
		case "info":
			globalLogLevel = LogLevel.info;
			break;
		case "trace":
			globalLogLevel = LogLevel.trace;
			break;
		case "warning":
			globalLogLevel = LogLevel.warning;
			break;
		case "off":
		default:
			globalLogLevel = LogLevel.off;
			break;
			
	}
	
	if(touch(logfile))
		sharedLog = new FileLogger(logfile);
}

bool touch(string file = "")
{
	if(!file.length)return false;
	if(exists(file))return true;
	
	auto info = split(file,"/");
	if(info.length > 1)
	{
		string path = join(info[0 .. $-1],"/");
		mkdirRecurse(path);
	}
	file.write(file,"");
	return true;
}
