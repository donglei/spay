## 聚合支付

### 平台要求
 - 系统：Debian or other Linux
 - 编译器： DMD 2.071及以上
 - 数据库：Postgresql
 - 队列服务：Beanstalkd
 
### 依赖库
 - Hunt version：v0.6.0-b2及以上
 - openssl version~>1.1.5+1.0.1g
 - dpq version ~>0.9.0-alpha.1
 
### 支付渠道Channel
 - Alipay 支付宝APP支付
 - AlipayWap 支付宝手机网页支付
 - WeixinPay 微信APP支付
 - WeixinPayQr 微信扫码支付
 - WeixinPayPub 微信公众号支付

### 异步通知推送策略
 
 每笔订单最多推送8次，延迟时间分别为：0s 4m 10m 10m 1h 2h 6h 15h;
 以返回`success`字符为处理成功,推送超过8次不在推送，可以通过query接口查询订单状态
 
### 接口文档
 - 创建支付订单接口
 - 支付订单状态查询接口
 
### 数据加密及签名
 - 数据使用AES（对称加密算法）`aes-256-cbc`, PKCS#7 协议补齐，加密秘钥Key是分配给应该的SecretKey；加密后使用base64_encode编码
 作为数据传输到服务端
 - 签名算法：md5（base64_encode_data + currentUnixTime + SecretKey）.toUpper
 
### 其他
 - 文档在doc目录下
 - notifyclient 目前下是异步通知处理程序