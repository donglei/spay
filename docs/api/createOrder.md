## 创建支付订单接口
    
**简要描述：** 

- 创建支付订单接口

**请求URL：** 
- ` /pay/order`
  
**请求方式：**
- POST 

**POST内容为AES加密后的Base64Encode 字符串：** 
- AES加密后的Base64Encode 字符串 
`oxf7jVp0mGtbQpdYJSgG+VLglJMB5d+/CuqKzwOSDh94RVOE9+NQHoICtPyfyG2oyUqICeJhj/yyGxAMOCUIDxO0jNZFO43tfSZepYhnsz6kUzKTonKBwyxobgdJ+X6LWqliwU+IEUKTLrfrFwJINR00adTQFVeC6/i5GLB8L8pSdXlG2j98fd1nWb4p/5FRWOe8Y4kw7jwVvwYO9I2Mw0Y+nnd6be/RPMw+MiBPLGkniiX/WoL6kT48byPKRFcfsoy/sj+ljk4F05/mPmEZ0I212EN5ND9eJPMAVWOjvQPH0GhjA8sdzJCuWJ+R2qUNIbL4b5Sqdfw4MXjeMjzXJCa9ybf0wA72YxTVYvElESflASy7gf+3vBjTPV6ocRvX93VwKGX4eTRgtWIwc/GGmU7G1wscsV/+uQ0zIw/hLblZSbMZXSTKOkKTUFK6pyW/P+JIpf6ScZbXcIWO0gGpUziO+H7MqNBTdKfXj9FSiEmM1xDTScq0WqVYWVcC/sH`
- 加密前的数据为请求参数的json string
`{"subject":"\u7ea2\u8272iPhone7 128G","body":"red apple made in China","amount":23,"order_no":"20170412045925T58edec6da1283","currency":"cny","extra":"{\"success_url\":\"http:\\\/\\\/10.1.223.224:8082\\\/alipaywapredirect.php\"}","channel":"AlipayWap","client_ip":"10.1.223.218","appid":"c1d4d06048ed7342726955367350baba","notify_url":"http:\/\/10.1.223.224:8082\/notify.php"}`

**Header中添加额外信息：**
- X-Spay-AppId 应用appid 如 `c1d4d06048ed7342726955367350baba`
- X-Spay-Request-Time 签名中使用到的currentUnixTime 如 `1491985738`
- X-Spay-Sign 根据签名规则生成的值 如 `f880a9c012b84c378bca49f9252a6f66`
- X-Spay-ApiVersion APIversion 值为一串字符串，用于收集客户端的一些信息
 
 
**请求参数**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|appid |是  |string | 应用编号    |
|channel     |是  |string | 支付渠道, 大小写敏感 详细见根目录的README中|
|subject |是  |string | 主体    |
|body |是  |string | 描述    |
|amount |是  |int | 金额 精确到分    |
|currency |是  |string | 货币类型 固定值：cny    |
|order_no |是  |string | 商户订单号    |
|client_ip |是  |string | 发起支付请求客户端的 IP 地址，格式为 IPV4，如: 127.0.0.1    |
|notify_url |是  |string | 支付成功后的异步回调地址   |
|passback_params |否  |string | 附加需要返回的参数 |
|success_url |否  |string | 支付渠道为`AlipayWap`时，支付成功的返回的页面 |
|open_id |否  |string | 支付渠道为`WeixinPayPub`时，需要提供授权的open_id |

 **返回示例**

 - 支付宝APP支付 WAP支付返回的示例

``` 

  {
      "bill_id": "1018090058886284288",
      "channel": "AlipayWap",
      "credential": {
          "app_id": "2016080300154304",
          "biz_content": "{\"body\":\"red apple made in China\",\"out_trade_no\":\"1018090058886284288\",\"passback_params\":\"c1d4d06048ed7342726955367350baba%40AlipayWap%4020170412044340T58ede8bca8531\",\"product_code\":\"QUICK_WAP_PAY\",\"subject\":\"红色iPhone7 128G\",\"total_amount\":\"0.29\"}",
          "charset": "utf-8",
          "format": "JSON",
          "method": "alipay.trade.wap.pay",
          "notify_url": "https:\/\/spay.putao.com\/v1\/alipay\/callbak\/c1d4d06048ed7342726955367350baba-AlipayWap",
          "return_url": "http:\/\/10.1.223.224:8082\/alipaywapredirect.php",
          "sign": "vUo\/zApegYRlw3sZHLMOe\/ebGPCNNxHCf26UhepgVyqCD6Tezp7rHY6QADWVEjAbKSkUfkp8LTOd070qKM0wN47Z41BvCNe7WwwhZiF1qdAjqVKk+aeYZT8nWutfIi2hJxDcxbqMK8bIA5mNZEAzsW+hMxyvnAa4TpL8ya5tItWC7vVA89dJJfS7z+QJXmeOt83jQihthh4hT6QNxn3lIZ1OYIR1ZvJdRhmoZSsD\/H1uZcmfwMLVCs71QCAI7wNZBUiH+mLaZ8zAuvgfO5O7nt184ftZCaAOou1Y99ssPTIxIsCSA+T3be+XTfGdXhjNIPgzWgwIOM4Zb\/gaYm0r6w==",
          "sign_type": "RSA2",
          "timestamp": "2017-04-12 16:43:40",
          "version": "1.0"
      }
  }
```

 - 微信APP支付返回的示例

 ```
  {
    "bill_id": "1018091360994214912",
    "channel": "WeixinPay",
    "credential": {
        "appid": "wx06a0e77b790bbc19",
        "noncestr": "t0YryasOi5XqXKLq",
        "package": "Sign=WXPay",
        "package_": "Sign=WXPay",
        "partnerid": "1308474501",
        "prepayid": "wx20170413144812d5311e4dd60253302950",
        "sign": "38C27F85BEECE933F5D73D79B318E58E",
        "timestamp": "1492066095"
    }
  }
 ```

 - 微信扫码支付返回的示例

 ```
 {
    "bill_id": "1018091360994214912",
    "channel": "WeixinPayQr",
    "credential": {
        "code_url": "weixin://wxpay/s/An4baqw"
    }
  }
 ```
 **返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|bill_id |string   | 生成的支付订单编号  |
|channel |string   | 支付渠道  |
|credential |Json object   | 支付渠道对应需要的具体数据  |

 **备注** 

- 更多返回错误代码请看首页的错误代码描述


