--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.0

-- Started on 2017-04-14 17:30:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 25643)
-- Name: pay_appinfo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pay_appinfo (
    appid character varying NOT NULL,
    name character varying,
    secret_key character varying,
    uid bigint,
    status integer
);


ALTER TABLE pay_appinfo OWNER TO postgres;

--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN pay_appinfo.secret_key; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pay_appinfo.secret_key IS '加密key';


--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN pay_appinfo.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pay_appinfo.uid IS '应用拥有者';


--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN pay_appinfo.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pay_appinfo.status IS '应用状态';


--
-- TOC entry 2107 (class 0 OID 25643)
-- Dependencies: 181
-- Data for Name: pay_appinfo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pay_appinfo (appid, name, secret_key, uid, status) FROM stdin;
c1d4d06048ed7342726955367350baba	开发应用	3234257aa9fe9a460ec5fde040abb53a	6666	1
\.


--
-- TOC entry 1992 (class 2606 OID 25650)
-- Name: pay_appinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pay_appinfo
    ADD CONSTRAINT pay_appinfo_pkey PRIMARY KEY (appid);


-- Completed on 2017-04-14 17:30:04

--
-- PostgreSQL database dump complete
--

