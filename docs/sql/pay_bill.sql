﻿-- Table: public.pay_bill

-- DROP TABLE public.pay_bill;

CREATE TABLE public.pay_bill
(
  id bigint NOT NULL,
  out_trade_no character varying,
  trade_no character varying,
  amount integer,
  channel character varying,
  appid character varying,
  create_time integer,
  subject character varying,
  body character varying,
  payment_time integer,
  refund_time integer,
  status integer DEFAULT 0, -- 0 未付款，1付款成功，2已退款
  passback_params character varying, -- 回调时 返回的数据
  notify_url character varying, -- 推送通知回调
  notify_success_time integer, -- 通知APP成功的时间
  CONSTRAINT "bill_pk-id" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.pay_bill
  OWNER TO postgres;
COMMENT ON COLUMN public.pay_bill.status IS '0 未付款，1付款成功，2已退款';
COMMENT ON COLUMN public.pay_bill.passback_params IS '回调时 返回的数据';
COMMENT ON COLUMN public.pay_bill.notify_url IS '推送通知回调';
COMMENT ON COLUMN public.pay_bill.notify_success_time IS '通知APP成功的时间';

