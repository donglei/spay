﻿-- Table: public.pay_appinfo

-- DROP TABLE public.pay_appinfo;

CREATE TABLE public.pay_appinfo
(
  appid character varying NOT NULL,
  name character varying,
  secret_key character varying, -- 加密key
  uid bigint, -- 应用拥有者
  status integer, -- 应用状态
  CONSTRAINT pay_appinfo_pkey PRIMARY KEY (appid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.pay_appinfo
  OWNER TO postgres;
COMMENT ON COLUMN public.pay_appinfo.secret_key IS '加密key';
COMMENT ON COLUMN public.pay_appinfo.uid IS '应用拥有者';
COMMENT ON COLUMN public.pay_appinfo.status IS '应用状态';

