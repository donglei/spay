﻿-- Table: public.pay_app_channel

-- DROP TABLE public.pay_app_channel;

CREATE TABLE public.pay_app_channel
(
  appid character varying NOT NULL,
  config xml, -- 配置信息为xml
  channel character varying NOT NULL,
  CONSTRAINT pay_app_channel_pkey PRIMARY KEY (appid, channel)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.pay_app_channel
  OWNER TO postgres;
COMMENT ON TABLE public.pay_app_channel
  IS '应用开通的支付渠道';
COMMENT ON COLUMN public.pay_app_channel.config IS '配置信息为xml';

